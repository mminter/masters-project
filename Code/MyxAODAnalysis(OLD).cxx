#include <AsgTools/MessageCheck.h>
#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include <MyAnalysis/MyxAODAnalysis.h>

#include <xAODEventInfo/EventInfo.h>

#include "xAODTruth/TruthEventContainer.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthVertexContainer.h"
#include "xAODTruth/TruthEvent.h"
#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/TruthVertex.h"

#include "xAODTracking/VertexContainer.h"
#include "xAODTracking/TrackParticle.h"
#include "xAODBPhys/BPhysHypoHelper.h"
#include "xAODTracking/TrackParticlexAODHelpers.h"
#include "xAODTracking/TrackingPrimitives.h"
#include "xAODBPhys/BPhysHelper.h"

#include "xAODParticleEvent/CompositeParticleContainer.h"
#include "xAODParticleEvent/CompositeParticleAuxContainer.h"

#include <PATInterfaces/CorrectionCode.h> // to check the return correction code status of tools
#include <xAODCore/ShallowAuxContainer.h>
#include <xAODCore/ShallowCopy.h>
#include "PathResolver/PathResolver.h"

#include <TFile.h>
#include <TVector3.h>
#include <vector>
//#include <deque>
#include <TMath.h>
#include <TSystem.h>

// this is needed to distribute the algorithm to the workers
ClassImp(MyxAODAnalysis)


class muTracksTool{
private:
    std::unordered_map<const xAOD::TrackParticle*, const xAOD::Muon*> m_Tracklookup;
public:
    muTracksTool(const xAOD::MuonContainer* importedMuonCollection){
       for(auto x : *importedMuonCollection){
          if(x->inDetTrackParticleLink().isValid()){
              m_Tracklookup[*(x->inDetTrackParticleLink())] = x;
              //std::cout << "added " << x->inDetTrackParticleLink().cachedElement() << " to " << x << std::endl;
          }
          //std::cout << "number of muons in map " << m_Tracklookup.size() << std::endl;
       }
    }

    int muonsWithTrack() const {
      return m_Tracklookup.size();
    }

    bool isMuon(const xAOD::TrackParticle* x) const {
      return m_Tracklookup.count(x);
    }

    const xAOD::Muon* getMuon(const xAOD::TrackParticle* x) const {
      auto it = m_Tracklookup.find(x);
      if(it != m_Tracklookup.end()) return it->second;
      return nullptr;
    }
};

MyxAODAnalysis :: MyxAODAnalysis () : m_muonSelection ("CP::MuonSelectionTool", this),
m_muonCalibrationAndSmearingTool ("CP::MuonCalibrationAndSmearingTool/MuonCorrectionTool"),
m_grl ("GoodRunsListSelectionTool/grl"),
m_trigConfigTool ("TrigConf::xAODConfigTool/xAODConfigTool"),
m_trigDecisionTool ("Trig::TrigDecisionTool/TrigDecisionTool")
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  Note that you should only put
  // the most basic initialization here, since this method will be
  // called on both the submission and the worker node.  Most of your
  // initialization code will go into histInitialize() and
  // initialize().
}



EL::StatusCode MyxAODAnalysis :: setupJob (EL::Job& job)
{
  // Here you put code that sets up the job on the submission object
  // so that it is ready to work with your algorithm, e.g. you can
  // request the D3PDReader service or add output files.  Any code you
  // put here could instead also go into the submission script.  The
  // sole advantage of putting it here is that it gets automatically
  // activated/deactivated when you add/remove the algorithm from your
  // job, which may or may not be of value to you.

  // let's initialize the algorithm to use the xAODRootAccess package
  job.useXAOD ();

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode MyxAODAnalysis :: histInitialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.

  TFile *outputFile = wk()->getOutputFile (m_outputName);
  mc_tree = new TTree ("mc_tree", "mc_tree");
  mc_tree->SetDirectory (outputFile);

  mc_tree->Branch ("evtNumber", &r_evtNumber);
  mc_tree->Branch ("runNumber", &r_runNumber);
  mc_tree->Branch ("deltaPhiTaus", &delta_phi_taus);
  mc_tree->Branch ("ptTauPlus", &pt_tau_plus);
  mc_tree->Branch ("ptTauMinus", &pt_tau_minus);

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode MyxAODAnalysis :: fileExecute ()
{
  // Here you do everything that needs to be done exactly once for every
  // single file, e.g. collect a list of all lumi-blocks processed
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode MyxAODAnalysis :: changeInput (bool /*firstFile*/)
{
  // Here you do everything you need to do when we change input files,
  // e.g. resetting branch addresses on trees.  If you are using
  // D3PDReader or a similar service this method is not needed.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode MyxAODAnalysis :: initialize ()
{
  // Here you do everything that you need to do after the first input
  // file has been connected and before the first event is processed,
  // e.g. create additional histograms based on which variables are
  // available in the input files.  You can also create all of your
  // histograms and trees in here, but be aware that this method
  // doesn't get called if no events are processed.  So any objects
  // you create here won't be available in the output if you have no
  // input events.
  ANA_MSG_INFO ("in initialize");

  // count number of events
  m_eventCounter = 0;
  m_trackCounter = 0;
  m_counter_eventsConsidered = 0;
  m_counter_eventsSelected = 0;
  m_counter_candidatesConsidered = 0;
  m_counter_candidatesSelected = 0;
  m_counter_eventsRejected = 0;

  ANA_CHECK (m_muonCalibrationAndSmearingTool.setProperty("Year", "Data17")); 
  ANA_CHECK (m_muonCalibrationAndSmearingTool.setProperty("Release", "Recs2017_08_02")); 
  ANA_CHECK (m_muonCalibrationAndSmearingTool.setProperty("SagittaRelease", "sagittaBiasDataAll_25_07_17")); 
  ANA_CHECK (m_muonCalibrationAndSmearingTool.setProperty("SagittaCorr", false)); // 2017
  //ANA_CHECK (m_muonCalibrationAndSmearingTool.setProperty("SagittaCorr", true)); // 2016
  ANA_CHECK (m_muonCalibrationAndSmearingTool.initialize());

  ANA_CHECK (m_muonSelection.setProperty("MaxEta", 2.7)); 
  ANA_CHECK (m_muonSelection.setProperty("UseAllAuthors", true));
  ANA_CHECK (m_muonSelection.setProperty("MuQuality", 5));
  ANA_CHECK (m_muonSelection.initialize());

  // Initialize and configure trigger tools
  ANA_CHECK (m_trigConfigTool.initialize());
  ANA_CHECK (m_trigDecisionTool.setProperty ("ConfigTool", m_trigConfigTool.getHandle())); // connect the TrigDecisionTool to the ConfigTool
  ANA_CHECK (m_trigDecisionTool.setProperty ("TrigDecisionKey", "xTrigDecision"));
  ANA_CHECK (m_trigDecisionTool.initialize());

  // GRL
  if (useGRL)
  {
      Info("initialize()", "use_grl = true (GRLs will be applied for data)");
      for(auto &str : grl_files){
          str = PathResolverFindCalibFile(str);
          Info("initialize()", str.c_str());
      }
   //   m_grl = new GoodRunsListSelectionTool("GoodRunsListSelectionTool");
      std::vector<std::string> vecStringGRL;
      vecStringGRL.push_back("/home/atlas/eva/3mu1/source/MyAnalysis/share/data12_8TeV.periodAllYear_DetStatus-v61-pro14-02_DQDefects-00-01-00_PHYS_StandardGRL_All_Good.xml");
      ANA_CHECK( m_grl.setProperty("GoodRunsListVec", vecStringGRL));
      ANA_CHECK( m_grl.setProperty("PassThrough", true)); // if true (default) will ignore result of GRL and will just pass all events
      //ANA_CHECK( m_grl.setProperty("GoodRunsListVec", grl_files));
      //ANA_CHECK( m_grl.setProperty("PassThrough", false)); // if true (default) will ignore result of GRL and will just pass all events
      ANA_CHECK( m_grl.initialize());
  }
  else
      Info("initialize()", "use_grl = false (GRLs will not be applied)"); 


  return EL::StatusCode::SUCCESS;
}



EL::StatusCode MyxAODAnalysis :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.

  //ANA_MSG_INFO ("in execute");

  // print every 1000 events, so we know where we are:
  //if( (m_eventCounter % 1000) == 0 ) Info("execute()", "Event number = %i", m_eventCounter );
  m_eventCounter++;

  // set type of return code you are expecting
  // (add to top of each function once)
  ANA_CHECK_SET_TYPE (EL::StatusCode);

  typedef ElementLink<xAOD::VertexContainer> VertexLink;
  typedef std::vector<VertexLink> VertexLinkVector;
  typedef ElementLink<xAOD::MuonContainer> MuonLink;
  typedef std::vector<MuonLink> MuonLinkVector;
  typedef ElementLink<xAOD::TrackParticleContainer> TrackParticleLink;
  typedef std::vector<TrackParticleLink> TrackParticleLinkVector;

  double CONST = 1000./299.792;

  // retrieve the eventInfo object from the event store
  const xAOD::EventInfo *eventInfo = nullptr;
  ANA_CHECK (evtStore()->retrieve (eventInfo, "EventInfo"));

  // print out run and event number from retrieved object
  ANA_MSG_INFO ("in execute, runNumber = " << eventInfo->runNumber() << ", eventNumber = " << eventInfo->eventNumber());
  //ANA_MSG_DEBUG ("in execute, runNumber = " << eventInfo->runNumber() << ", eventNumber = " << eventInfo->eventNumber());
  //unsigned long long evtNum = eventInfo->eventNumber();

  // check if the event is data or MC
  bool isMC = false;
  // check if the event is MC
  if (eventInfo->eventType (xAOD::EventInfo::IS_SIMULATION)) {
    isMC = true; // can do something with this later
  }

  // if data check if event passes GRL
  if (!isMC) { // it's data!
    if (!m_grl->passRunLB(*eventInfo)) {
      ANA_MSG_INFO ("drop event: GRL");
      return StatusCode::SUCCESS; // go to next event
    }
  } // end if not MC
  //ANA_MSG_INFO ("keep event: GRL");

  // TRACKS
  const xAOD::TrackParticleContainer* tracks = 0;
  ANA_CHECK(evtStore()->retrieve( tracks, "InDetTrackParticles"));
  ANA_MSG_INFO ("number of tracks " << tracks->size());

  // MUONS
  const xAOD::MuonContainer* muons = 0;
  ANA_CHECK(evtStore()->retrieve( muons, "Muons"));
  ANA_MSG_DEBUG ("number of muons " << muons->size());
  muTracksTool mutool(muons);

  // create a shallow copy of the muons container
  auto muons_shallowCopy = xAOD::shallowCopyContainer( *muons );
  std::unique_ptr<xAOD::MuonContainer> muonsSC (muons_shallowCopy.first);
  std::unique_ptr<xAOD::ShallowAuxContainer> muonsAuxSC (muons_shallowCopy.second);
  // iterate over our shallow copy
  int iSC = 0;
  for (auto muonSC : *muonsSC) {
    ANA_MSG_DEBUG ("original SC muon pt = " << muonSC->pt()*0.001 << " eta " << muonSC->eta() << " phi " << muonSC->phi());
    if (m_muonCalibrationAndSmearingTool->applyCorrection(*muonSC)!= CP::CorrectionCode::Ok) {
      ANA_MSG_INFO ("execute(): Problem with Muon Calibration And Smearing Tool (Error or OutOfValidityRange) ");
    }
    int mucalib_quality = static_cast<int>(m_muonSelection->getQuality(*muonSC));
    int mu_pass_softMu = (m_muonSelection->accept((*muonSC)) ? 1 : 0);
    // int mu_pass_softMu = (m_muonSelection.accept((muonSC ? *muonSC : *mu)) ? 1 : 0);
    ANA_MSG_DEBUG ("corrected   muon pt = " << muonSC->pt()*0.001 << " eta " << muonSC->eta() << " phi " << muonSC->phi() << " ID pt = " << muonSC->auxdata< float >( "InnerDetectorPt" )*0.001 << " MS pt = " << muonSC->auxdata< float >( "MuonSpectrometerPt" )*0.001 << " quality " << muonSC->quality() << " mucalib_quality " << mucalib_quality << " mu_pass_softMu " << mu_pass_softMu);
    int iO = 0;
    for (auto mu: *muons) {
      if (iSC == iO) {
       mu->auxdecor<float>("mucalib_pt") = muonSC->pt();
       mu->auxdecor<float>("mucalib_phi") = muonSC->phi();
       mu->auxdecor<float>("mucalib_eta") = muonSC->eta();
       mu->auxdecor<float>("mucalib_MSpt") = muonSC->auxdata< float >( "MuonSpectrometerPt" );
       mu->auxdecor<float>("mucalib_IDpt") = muonSC->auxdata< float >( "InnerDetectorPt" );
       mu->auxdecor<float>("mucalib_quality") = mucalib_quality;
       mu->auxdecor<float>("mucalib_pass_softMu") = mu_pass_softMu;
      }
      iO++;
    }
    iSC++;
  }


  // PRIMARY VERTICES
  const xAOD::VertexContainer* pvs = 0;
  ANA_CHECK(evtStore()->retrieve( pvs, "PrimaryVertices"));
  ANA_MSG_INFO ("number of PV " << pvs->size());
  unsigned int vcounter(0);
  float pv0_x = -999., pv0_y = -999., pv0_z = -999.;
  int pv0_n = -1;
  xAOD::Vertex* pv0(0);
  for (auto pv: *pvs) {
    if (vcounter == 0) {
      pv0 = pv;
      pv0_x = pv->x();
      pv0_y = pv->y();
      pv0_z = pv->z();
      pv0_n = pv->trackParticleLinks().size();
      vcounter++;
      continue;
    }
  }
  ANA_MSG_INFO ("PV0 x,y,z " << pv0_x << " " << pv0_y  << " " << pv0_z);

  // MC truth
  // TRUTH EVENTS
  const xAOD::TruthEventContainer* truthevs = 0;
  if (isMC) ANA_CHECK(evtStore()->retrieve( truthevs, "TruthEvents"));

  // TRUTH VERTICES
  const xAOD::TruthVertexContainer* truthvs = 0;
  if (isMC) ANA_CHECK(evtStore()->retrieve( truthvs, "TruthVertices"));

  // TRUTH PARTICLES
  const xAOD::TruthParticleContainer* truthps = 0;
  if (isMC) ANA_CHECK(evtStore()->retrieve( truthps, "TruthParticles"));

  int isSignal = 0, isReco = 0;

  //Trk::GlobalPosition gpMCtauMinus;
  //Trk::GlobalPosition gpMCtauPlus;
  //Trk::GlobalPosition gpMCBoson;
  //Trk::RecVertex* vtxZ=0;

  float xPlusMC = 0., yPlusMC = 0., zPlusMC = 0.;
  float xMinusMC = 0., yMinusMC = 0., zMinusMC = 0.;
  float nu_tauPlusPxMC = 0., nu_tauPlusPyMC = 0., nu_tauPlusPzMC = 0.;
  float nu_tauMinusPxMC = 0., nu_tauMinusPyMC = 0., nu_tauMinusPzMC = 0.;
  float miss_Ex = 0., miss_Ey = 0., miss_Et = 0.;
  float Px_plusMC = 0., Py_plusMC = 0., Pz_plusMC = 0., E_plusMC = 0.;
  float Px_plusMC3 = 0., Py_plusMC3 = 0., Pz_plusMC3 = 0., E_plusMC3 = 0.;
  float Px_minusMC = 0., Py_minusMC = 0., Pz_minusMC = 0., E_minusMC = 0.;
  float Px_minusMC3 = 0., Py_minusMC3 = 0., Pz_minusMC3 = 0., E_minusMC3 = 0.;

  const xAOD::TruthParticle* Boson=0;
  const xAOD::TruthParticle* nu_tauMinus=0;
  const xAOD::TruthParticle* nu_tauPlus=0;
  const xAOD::TruthParticle* tauMinus=0;
  const xAOD::TruthParticle* tauPlus=0;
  const xAOD::TruthParticle* tauMinusChild1=0;
  const xAOD::TruthParticle* tauMinusChild2=0;
  const xAOD::TruthParticle* tauMinusChild3=0;
  const xAOD::TruthParticle* tauPlusChild1=0;
  const xAOD::TruthParticle* tauPlusChild2=0;
  const xAOD::TruthParticle* tauPlusChild3=0;
  const xAOD::TruthParticle* tauMinus0=0;
  const xAOD::TruthParticle* tauPlus0=0;
  const xAOD::TrackParticle* tauMinusChild1TP(0);
  const xAOD::TrackParticle* tauMinusChild2TP(0);
  const xAOD::TrackParticle* tauMinusChild3TP(0);
  const xAOD::TrackParticle* tauPlusChild1TP(0);
  const xAOD::TrackParticle* tauPlusChild2TP(0);
  const xAOD::TrackParticle* tauPlusChild3TP(0);

  //std::vector<CLHEP::HepVector> nuMomentumPlusMC_vec;
  //std::vector<CLHEP::HepVector> nuMomentumMinusMC_vec;
  //std::vector<CLHEP::HepVector> nuMomentumPlusMC3_vec;
  //std::vector<CLHEP::HepVector> nuMomentumMinusMC3_vec;
  //CLHEP::HepLorentzVector Z11MC, Z12MC, Z21MC, Z22MC, Z11MC3, Z12MC3, Z21MC3, Z22MC3;
  //CLHEP::HepVector nuMomentumPlus1MC(3,0);
  //CLHEP::HepVector nuMomentumPlus2MC(3,0);
  //CLHEP::HepVector nuMomentumMinus1MC(3,0);
  //CLHEP::HepVector nuMomentumMinus2MC(3,0);
  //CLHEP::HepVector nuMomentumPlus1MC3(3,0);
  //CLHEP::HepVector nuMomentumPlus2MC3(3,0);
  //CLHEP::HepVector nuMomentumMinus1MC3(3,0);
  //CLHEP::HepVector nuMomentumMinus2MC3(3,0);
  int mc_sol = 0, mc_sol3 = 0;

  double zrapidity = -99999.;
  int npions_tau = 0., nphoton_tau = 0., npi0_tau = 0., nkaons_tau = 0., nk0_tau = 0., nk0l_tau = 0., nk0s_tau = 0.;
  int npions_anti_tau = 0., nphoton_anti_tau = 0., npi0_anti_tau = 0., nkaons_anti_tau = 0., nk0_anti_tau = 0.,  nk0l_anti_tau = 0., nk0s_anti_tau = 0.;
  int sizePlusMC = 0., sizeMinusMC = 0., sizePlusMC3 = 0., sizeMinusMC3 = 0., n_taus = 0., n_anti_taus = 0.;
  double tau_x_plus = 0., tau_y_plus = 0., tau_z_plus = 0., tau_x_minus = 0., tau_y_minus = 0., tau_z_minus = 0., ctau_plusMC = 0., ctau_minusMC = 0.;
  double px_photon_plus = 0., py_photon_plus = 0., pz_photon_plus = 0., pe_photon_plus = 0.;
  double px_photon_minus = 0., py_photon_minus = 0., pz_photon_minus = 0., pe_photon_minus = 0.;
  double px_pi0_plus = 0., py_pi0_plus = 0., pz_pi0_plus = 0., px_pi0_minus = 0., py_pi0_minus = 0., pz_pi0_minus = 0.;
  double tau_px_plus = 0., tau_py_plus = 0., tau_pz_plus = 0., tau_px_minus = 0., tau_py_minus = 0., tau_pz_minus = 0.;
  double vis_px_plus = 0., vis_py_plus = 0., vis_pz_plus = 0., vis_e_plus = 0., anti_nu_tau_mass = 0.;
  double vis_px_minus = 0., vis_py_minus = 0., vis_pz_minus = 0., vis_e_minus = 0., nu_tau_mass = 0.;
  double anti_nu_tau_px = 0., anti_nu_tau_py = 0., anti_nu_tau_pz = 0., nu_tau_px = 0., nu_tau_py = 0., nu_tau_pz = 0.; 
  double px_k0_minus = 0., py_k0_minus = 0., pz_k0_minus = 0., pe_k0_minus = 0., px_k0_plus = 0., py_k0_plus = 0., pz_k0_plus = 0., pe_k0_plus = 0.;
  double px_k0l_minus = 0., py_k0l_minus = 0., pz_k0l_minus = 0., pe_k0l_minus = 0., px_k0l_plus = 0., py_k0l_plus = 0., pz_k0l_plus = 0., pe_k0l_plus = 0.;
  double px_k0s_minus = 0., py_k0s_minus = 0., pz_k0s_minus = 0., pe_k0s_minus = 0., px_k0s_plus = 0., py_k0s_plus = 0., pz_k0s_plus = 0., pe_k0s_plus = 0.;
  double dx_plusMC = 0., dy_plusMC = 0., dz_plusMC = 0., dx_minusMC = 0., dy_minusMC = 0., dz_minusMC = 0.;
  bool signal_chain =  false;
  bool signal_chain3 =  false; 
  bool p3decay = false;

  double rec_pt_tauMinusChild1 = -1., rec_phi_tauMinusChild1 = -9., rec_eta_tauMinusChild1 = -9., rec_charge_tauMinusChild1 = 0.;
  double rec_pt_tauMinusChild2 = -1., rec_phi_tauMinusChild2 = -9., rec_eta_tauMinusChild2 = -9., rec_charge_tauMinusChild2 = 0.;
  double rec_pt_tauMinusChild3 = -1., rec_phi_tauMinusChild3 = -9., rec_eta_tauMinusChild3 = -9., rec_charge_tauMinusChild3 = 0.;
  double rec_pt_tauPlusChild1 = -1., rec_phi_tauPlusChild1 = -9., rec_eta_tauPlusChild1 = -9., rec_charge_tauPlusChild1 = 0.;
  double rec_pt_tauPlusChild2 = -1., rec_phi_tauPlusChild2 = -9., rec_eta_tauPlusChild2 = -9., rec_charge_tauPlusChild2 = 0.;
  double rec_pt_tauPlusChild3 = -1., rec_phi_tauPlusChild3 = -9., rec_eta_tauPlusChild3 = -9., rec_charge_tauPlusChild3 = 0.;
  int rec_tauMinusChild1 = 0, rec_tauMinusChild2 = 0, rec_tauMinusChild3 = 0, rec_tauPlusChild1 = 0, rec_tauPlusChild2 = 0, rec_tauPlusChild3 = 0;

  if (isMC) {
    xAOD::TruthEventContainer::const_iterator itr;
    for (itr = truthevs->begin(); itr!=truthevs->end(); ++itr) {
      xAOD::TruthParticleContainer::const_iterator truthp_itr = truthps->begin();
      xAOD::TruthParticleContainer::const_iterator truthp_end = truthps->end();
      for ( ; truthp_itr != truthp_end; ++truthp_itr ) {
        if ((*truthp_itr)->pdgId() ==  15 && (*truthp_itr)->status() == m_tau_status) n_taus++;
        if ((*truthp_itr)->pdgId() == -15 && (*truthp_itr)->status() == m_tau_status) n_anti_taus++;

        //ANA_MSG_INFO ("pdgId = " << (*truthp_itr)->pdgId() << " status " << (*truthp_itr)->status());
        //if ( (*truthp_itr)->pdgId() == m_pid) ANA_MSG_INFO ("Boson pdgId = " << (*truthp_itr)->pdgId() << " status " << (*truthp_itr)->status() << " momentum " << (*truthp_itr)->p4().Px() << ", " << (*truthp_itr)->p4().Py() << ", " << (*truthp_itr)->p4().Pz());
        if ( (*truthp_itr)->pdgId() == m_pid && (*truthp_itr)->status() == m_status && (*truthp_itr)->hasDecayVtx() )
        {
          Boson = (*truthp_itr);
          size_t nch = ((*truthp_itr)->decayVtx())->nOutgoingParticles();
          ANA_MSG_INFO ("Boson nOutgoingParticles " << nch);
	  const xAOD::TruthVertex * d_vertex = (*truthp_itr)->decayVtx();
          int nzchildren = 0;
          for (size_t i_child = 0; i_child < nch; i_child++) {
            nzchildren++;
            const xAOD::TruthParticle* daughter = d_vertex->outgoingParticle(i_child);
            ANA_MSG_INFO ("daughter id " << daughter->pdgId() << " status " << daughter->status());
            if (!daughter->hasDecayVtx()) ANA_MSG_INFO ("daughter has no decay vertex");
            if (daughter->hasDecayVtx()) ANA_MSG_INFO ("daughter nOutgoingParticles " << (daughter->decayVtx())->nOutgoingParticles());
            ATH_MSG_INFO("Boson child " << daughter->pdgId() << " status " << daughter->status() << " momentum " << daughter->p4().Px() << ", " << daughter->p4().Py() << ", " << daughter->p4().Pz() << ", " << daughter->p4().E() << " mass " << daughter->p4().M()  << " barcode " << daughter->barcode());
            if (daughter->pdgId() == 15 && daughter->status() != 2) tauMinus0 = daughter;
            if (daughter->pdgId() == -15 && daughter->status() != 2) tauPlus0 = daughter;

            const xAOD::TruthParticle* test_mc_tau_minus(0);
            if (daughter->pdgId() == 15 && daughter->status() == 2) test_mc_tau_minus = daughter;
            if (daughter->pdgId() == 15 && daughter->status() != 2) {
              const xAOD::TruthParticle* testParticle = mc_daughter(daughter);
              ANA_MSG_INFO ("testParticle id " << testParticle->pdgId() << " status " << testParticle->status() << " hasDecayVertex " << testParticle->hasDecayVtx() << " momentum " << testParticle->p4().Px() << ", " << testParticle->p4().Py() << ", " << testParticle->p4().Pz() << ", " << testParticle->p4().E() << " mass " << testParticle->p4().M()  << " barcode " << testParticle->barcode());
              while (testParticle != NULL && testParticle->status() != 2) {
                if ( mc_daughter(testParticle) != NULL ) {
                  testParticle = mc_daughter(testParticle);
                } else {
                  testParticle = NULL; // ok, you can stop now.
                }
              }
              if (testParticle) test_mc_tau_minus = testParticle;
            }
            if (test_mc_tau_minus) ANA_MSG_INFO ("test_mc_tau_minus id " << test_mc_tau_minus->pdgId() << " status " << test_mc_tau_minus->status() << " hasDecayVertex " << test_mc_tau_minus->hasDecayVtx() << " momentum " << test_mc_tau_minus->p4().Px() << ", " << test_mc_tau_minus->p4().Py() << ", " << test_mc_tau_minus->p4().Pz() << ", " << test_mc_tau_minus->p4().E() << " mass " << test_mc_tau_minus->p4().M()  << " barcode " << test_mc_tau_minus->barcode());

            if (test_mc_tau_minus)
            {
              tauMinus = test_mc_tau_minus;
              size_t dnch = (test_mc_tau_minus->decayVtx())->nOutgoingParticles();
              const xAOD::TruthVertex * gd_vertex = test_mc_tau_minus->decayVtx();
              for (size_t i_gchild = 0; i_gchild < dnch; i_gchild++) {
                const xAOD::TruthParticle* gdaughter = gd_vertex->outgoingParticle(i_gchild);
                ANA_MSG_INFO ("tauMinus daughter id " << gdaughter->pdgId() << " status " << gdaughter->status());
                if (abs(gdaughter->pdgId()) == 211 && gdaughter->status() == 1) {
                  npions_tau++;
                  if (npions_tau == 1) tauMinusChild1 = gdaughter;
                  if (npions_tau == 2) tauMinusChild2 = gdaughter;
                  if (npions_tau == 3) tauMinusChild3 = gdaughter;
                  vis_px_minus += gdaughter->p4().Px();
                  vis_py_minus += gdaughter->p4().Py();
                  vis_pz_minus += gdaughter->p4().Pz();
                  vis_e_minus  += gdaughter->p4().E();
                }
                if (abs(gdaughter->pdgId()) == 321 && gdaughter->status() == 1) {
                  nkaons_tau++;
                  vis_px_minus += gdaughter->p4().Px();
                  vis_py_minus += gdaughter->p4().Py();
                  vis_pz_minus += gdaughter->p4().Pz();
                  vis_e_minus  += gdaughter->p4().E();
		}
                if (gdaughter->pdgId() == 16 && gdaughter->status() == 1) {
                  nu_tauMinus = gdaughter;
                  nu_tau_px   = gdaughter->p4().Px();
                  nu_tau_py   = gdaughter->p4().Py();
                  nu_tau_pz   = gdaughter->p4().Pz();
                  nu_tau_mass = gdaughter->p4().M(); // Make larger
		}
                if (gdaughter->pdgId() == 111) {
                  npi0_tau++;
                  px_pi0_minus += gdaughter->p4().Px();
                  py_pi0_minus += gdaughter->p4().Py();
                  pz_pi0_minus += gdaughter->p4().Pz();
		}
                if (gdaughter->pdgId() == 22 && gdaughter->status() == 1) {
                  nphoton_tau++;
                  px_photon_minus += gdaughter->p4().Px();
                  py_photon_minus += gdaughter->p4().Py();
                  pz_photon_minus += gdaughter->p4().Pz();
                  pe_photon_minus += gdaughter->p4().E();
		}
		if (fabs(gdaughter->pdgId() == 311) && gdaughter->status() == 1) {
		  nk0_tau++;
		  px_k0_minus += gdaughter->p4().Px();
                  py_k0_minus += gdaughter->p4().Px();
                  pz_k0_minus += gdaughter->p4().Px();
                  pe_k0_minus += gdaughter->p4().E();
		}
		if (fabs(gdaughter->pdgId() == 130) && gdaughter->status() == 1) {
		  nk0l_tau++;
		  px_k0l_minus += gdaughter->p4().Px();
		  py_k0l_minus += gdaughter->p4().Px();
		  pz_k0l_minus += gdaughter->p4().Px();
		  pe_k0l_minus += gdaughter->p4().E();
		}
		if (fabs(gdaughter->pdgId() == 310) && gdaughter->status() == 1) {
                  nk0s_tau++;
                  px_k0s_minus += gdaughter->p4().Px();
                  py_k0s_minus += gdaughter->p4().Px();
                  pz_k0s_minus += gdaughter->p4().Px();
                  pe_k0s_minus += gdaughter->p4().E();
		}	    
	      }
	      int prong_minus = npions_tau + nkaons_tau;
	      if (prong_minus == 1) {
		if (npions_tau == 1) {
		  if (npi0_tau == 0) ANA_MSG_INFO ("This is a 1-prong decay, containing a charged pion");
                  if (npi0_tau != 0) ANA_MSG_INFO ("This is a 1-prong decay, containing a charged pion and neutral pion(s)");
                  if (nk0_tau != 0 || nk0l_tau != 0 || nk0s_tau != 0) ANA_MSG_INFO ("This is a 1-prong decay, containing a charged pion and neutral kaon(s)");
                }
                if (nkaons_tau == 1) {
                  if (npi0_tau == 0) ANA_MSG_INFO ("This is a 1-prong decay, containing a charged kaon");
                  if (npi0_tau != 0) ANA_MSG_INFO ("This is a 1-prong decay, containing a charged kaon and neutral pion(s)");
                  if (nk0_tau != 0 || nk0l_tau != 0 || nk0s_tau != 0) ANA_MSG_INFO ("This is a 1-prong decay, containing a charged kaon and neutral kaon(s)");
                }
              } // 1 prong                                                                                                                                                                                                                  
              if (prong_minus == 3) {
		if (npions_tau != 0) {
                  if (nkaons_tau == 0 && npi0_tau == 0) ANA_MSG_INFO ("This is a 3-prong decay, containing 3 charged pions");
                  if (nkaons_tau == 0 && npi0_tau != 0) ANA_MSG_INFO ("This is a 3-prong decay, containing 3 charged pions and neutral pion(s)");
                  if (nkaons_tau != 0) ANA_MSG_INFO ("This is a 3-prong decay, containing charged pion(s) and kaon(s)");
                  if (nkaons_tau != 0 && npi0_tau != 0) ANA_MSG_INFO ("This is a 3-prong decay, containing charged pion(s), charged kaon(s) and neutral pion(s)");
                }
                if (nkaons_tau != 0 && npions_tau == 0) ANA_MSG_INFO ("This is a 3-prong decay, containing kaons");
              } // 3 prong  
	      if (tauMinus) ANA_MSG_INFO ("tauMinus: npions_tau " << npions_tau << " nkaons_tau " << nkaons_tau << " npi0_tau " << npi0_tau << " nphoton_tau " << nphoton_tau << " nk0_tau " << nk0_tau << " nk0l " << nk0l_tau << " nk0s_tau " << nk0l_tau);
            } // END OF TAU MINUS LOOP

            const xAOD::TruthParticle* test_mc_tau_plus(0);
            if (daughter->pdgId() == -15 && daughter->status() == 2) test_mc_tau_plus = daughter;
            if (daughter->pdgId() == -15 && daughter->status() != 2) {
              const xAOD::TruthParticle* testParticle = mc_daughter(daughter);
              ANA_MSG_INFO ("testParticle id " << testParticle->pdgId() << " status " << testParticle->status() << " hasDecayVertex " << testParticle->hasDecayVtx() << " momentum " << testParticle->p4().Px() << ", " << testParticle->p4().Py() << ", " << testParticle->p4().Pz() << ", " << testParticle->p4().E() << " mass " << testParticle->p4().M()  << " barcode " << testParticle->barcode());
              while (testParticle != NULL && testParticle->status() != 2) {
                if ( mc_daughter(testParticle) != NULL ) {
                  testParticle = mc_daughter(testParticle);
                } else {
                  testParticle = NULL; // ok, you can stop now.
                }
              }
              if (testParticle) test_mc_tau_plus = testParticle;
            }
            if (test_mc_tau_plus) ANA_MSG_INFO ("test_mc_tau_plus id " << test_mc_tau_plus->pdgId() << " status " << test_mc_tau_plus->status() << " hasDecayVertex " << test_mc_tau_plus->hasDecayVtx() << " momentum " << test_mc_tau_plus->p4().Px() << ", " << test_mc_tau_plus->p4().Py() << ", " << test_mc_tau_plus->p4().Pz() << ", " << test_mc_tau_plus->p4().E() << " mass " << test_mc_tau_plus->p4().M()  << " barcode " << test_mc_tau_plus->barcode());

            
            if (test_mc_tau_plus) // TAU PLUS LOOP
            {
              tauPlus = test_mc_tau_plus;
              size_t dnch = (test_mc_tau_plus->decayVtx())->nOutgoingParticles();
              const xAOD::TruthVertex * gd_vertex = test_mc_tau_plus->decayVtx();
              for (size_t i_gchild = 0; i_gchild < dnch; i_gchild++) {
                const xAOD::TruthParticle* gdaughter = gd_vertex->outgoingParticle(i_gchild);
                ANA_MSG_INFO ("tauPlus daughter id " << gdaughter->pdgId() << " status " << gdaughter->status());
                if (abs(gdaughter->pdgId()) == 211 && gdaughter->status() == 1) {
                  npions_anti_tau++;
                  if (npions_anti_tau == 1) tauPlusChild1 = gdaughter;
                  if (npions_anti_tau == 2) tauPlusChild2 = gdaughter;
                  if (npions_anti_tau == 3) tauPlusChild3 = gdaughter;
                  vis_px_plus += gdaughter->p4().Px();
                  vis_py_plus += gdaughter->p4().Py();
                  vis_pz_plus += gdaughter->p4().Pz();
                  vis_e_plus  += gdaughter->p4().E();
                }
                if (abs(gdaughter->pdgId()) == 321 && gdaughter->status() == 1) {
                  nkaons_anti_tau++;
                  vis_px_plus += gdaughter->p4().Px();
                  vis_py_plus += gdaughter->p4().Py();
                  vis_pz_plus += gdaughter->p4().Pz();
                  vis_e_plus  += gdaughter->p4().E();
                }
                if (gdaughter->pdgId() == 16 && gdaughter->status() == 1) {
                  nu_tauPlus = gdaughter;
                  anti_nu_tau_px   = gdaughter->p4().Px();
                  anti_nu_tau_py   = gdaughter->p4().Py();
                  anti_nu_tau_pz   = gdaughter->p4().Pz();
                  anti_nu_tau_mass = gdaughter->p4().M();
                }
                if (gdaughter->pdgId() == 111) {
                  npi0_anti_tau++;
                  px_pi0_plus += gdaughter->p4().Px();
                  py_pi0_plus += gdaughter->p4().Py();
                  pz_pi0_plus += gdaughter->p4().Pz();
                }
		if (gdaughter->pdgId() == 22
                    && gdaughter->status() == 1) {
                  nphoton_anti_tau++;
                  px_photon_plus += gdaughter->p4().Px();
                  py_photon_plus += gdaughter->p4().Py();
                  pz_photon_plus += gdaughter->p4().Pz();
                  pe_photon_plus += gdaughter->p4().E();
		}
		if (fabs(gdaughter->pdgId() == 311) && gdaughter->status() == 1) {
                  nk0_anti_tau++;
                  px_k0_plus += gdaughter->p4().Px();
                  py_k0_plus += gdaughter->p4().Px();
                  pz_k0_plus += gdaughter->p4().Px();
                  pe_k0_plus += gdaughter->p4().E();
                }
		if (fabs(gdaughter->pdgId() == 130) && gdaughter->status() == 1) {
		  nk0l_anti_tau++;
                  px_k0l_plus += gdaughter->p4().Px(); 
                  py_k0l_plus += gdaughter->p4().Px();	
                  pz_k0l_plus += gdaughter->p4().Px();
		  pe_k0l_plus += gdaughter->p4().E();
		}
		if (fabs(gdaughter->pdgId() == 310) && gdaughter->status() == 1) {
                  nk0s_anti_tau++;
                  px_k0s_plus += gdaughter->p4().Px();
                  py_k0s_plus += gdaughter->p4().Px();
                  pz_k0s_plus += gdaughter->p4().Px();
                  pe_k0s_plus += gdaughter->p4().E();
		}
	      }
	      int prong_plus = npions_anti_tau + nkaons_anti_tau;
	      if (prong_plus == 1) {
		if (npions_anti_tau == 1) { 
		  if (npi0_anti_tau == 0) ANA_MSG_INFO ("This is a 1-prong decay, containing a charged pion");
		  if (npi0_anti_tau != 0) ANA_MSG_INFO ("This is a 1-prong decay, containing a charged pion and neutral pion(s)"); 
		  if (nk0_anti_tau != 0 || nk0l_anti_tau != 0 || nk0s_anti_tau != 0) ANA_MSG_INFO ("This is a 1-prong decay, containing a charged pion and neutral kaon(s)"); 
		}
		if (nkaons_anti_tau == 1) { 
		  if (npi0_anti_tau == 0) ANA_MSG_INFO ("This is a 1-prong decay, containing a charged kaon");
		  if (npi0_anti_tau != 0) ANA_MSG_INFO ("This is a 1-prong decay, containing a charged kaon and neutral pion(s)"); 
		  if (nk0_anti_tau != 0 || nk0l_anti_tau != 0 || nk0s_anti_tau != 0) ANA_MSG_INFO ("This is a 1-prong decay, containing a charged kaon and neutral kaon(s)"); 	
		}
	      } // 1 prong
	      if (prong_plus == 3) {
		if (npions_anti_tau != 0) {
		  if (nkaons_anti_tau == 0 && npi0_anti_tau == 0) ANA_MSG_INFO ("This is a 3-prong decay, containing 3 charged pions"); 
		  if (nkaons_anti_tau == 0 && npi0_anti_tau != 0) ANA_MSG_INFO ("This is a 3-prong decay, containing 3 charged pions and neutral pion(s)"); 
	 	  if (nkaons_anti_tau != 0) ANA_MSG_INFO ("This is a 3-prong decay, containing charged pion(s) and kaon(s)");
	 	  if (nkaons_anti_tau != 0 && npi0_anti_tau != 0) ANA_MSG_INFO ("This is a 3-prong decay, containing charged pion(s), charged kaon(s) and neutral pion(s)");
		}
		if (nkaons_anti_tau != 0 && npions_anti_tau == 0) ANA_MSG_INFO ("This is a 3-prong decay, containing kaons"); 	
	      } // 3 prong 
	      if (tauPlus) ANA_MSG_INFO ("tauPlus: npions_anti_tau " << npions_anti_tau << " nkaons_anti_tau " << nkaons_anti_tau << " npi0_anti_tau " << npi0_anti_tau << " nphoton_anti_tau " << nphoton_anti_tau << " nk0l_anti_tau " << nk0l_anti_tau << " nk0s_anti_tau " << nk0s_anti_tau);
            } // END OF TAU PLUS LOOP

          }
          //}

          ATH_MSG_INFO("nzchildren " << nzchildren);
        }

      }
      ATH_MSG_INFO("n_taus " << n_taus << " n_anti_taus " << n_anti_taus);  

    } // loop over truth events

    if ( Boson) ANA_MSG_INFO ("finally Boson pdgId = " << Boson->pdgId() << " status " << Boson->status() << " momentum " << Boson->p4().Px() << ", " << Boson->p4().Py() << ", " << Boson->p4().Pz());
    if (tauMinus0) ANA_MSG_INFO ("tauMinus0 id " << tauMinus0->pdgId() << " status " << tauMinus0->status() << " momentum " << tauMinus0->p4().Px() << ", " << tauMinus0->p4().Py() << ", " << tauMinus0->p4().Pz() << ", " << tauMinus0->p4().E() << " mass " << tauMinus0->p4().M());
    if (tauMinus) ANA_MSG_INFO ("tauMinus  id " << tauMinus->pdgId() << " status " << tauMinus->status() << " momentum " << tauMinus->p4().Px() << ", " << tauMinus->p4().Py() << ", " << tauMinus->p4().Pz() << ", " << tauMinus->p4().E() << " mass " << tauMinus->p4().M());
    if (tauPlus0) ANA_MSG_INFO ("tauPlus0 id " << tauPlus0->pdgId() << " status " << tauPlus0->status() << " momentum " << tauPlus0->p4().Px() << ", " << tauPlus0->p4().Py() << ", " << tauPlus0->p4().Pz() << ", " << tauPlus0->p4().E() << " mass " << tauPlus0->p4().M());
    if (tauPlus) ANA_MSG_INFO ("tauPlus  id " << tauPlus->pdgId() << " status " << tauPlus->status() << " momentum " << tauPlus->p4().Px() << ", " << tauPlus->p4().Py() << ", " << tauPlus->p4().Pz() << ", " << tauPlus->p4().E() << " mass " << tauPlus->p4().M());

    double x0 = -9999., y0 = -9999., z0 = -9999.;
    if (Boson) {
      const xAOD::TruthVertex * prod_vertex = Boson->prodVtx();
      x0 = prod_vertex->x();
      y0 = prod_vertex->y();
      z0 = prod_vertex->z();
    }

    if (tauMinus) {
      tau_px_minus = tauMinus->p4().Px();
      tau_py_minus = tauMinus->p4().Py();
      tau_pz_minus = tauMinus->p4().Pz();
      tau_x_minus = tauMinus->decayVtx()->x();
      tau_y_minus = tauMinus->decayVtx()->y();
      tau_z_minus = tauMinus->decayVtx()->z();
      dx_minusMC = tauMinus->decayVtx()->x() - Boson->decayVtx()->x();
      dy_minusMC = tauMinus->decayVtx()->y() - Boson->decayVtx()->y();
      dz_minusMC = tauMinus->decayVtx()->z() - Boson->decayVtx()->z();
      double drxy_minusMCsq = dx_minusMC*dx_minusMC + dy_minusMC*dy_minusMC;
      double drxy_minusMC = (drxy_minusMCsq>0.) ? sqrt(drxy_minusMCsq) : 0;
      ctau_minusMC = CONST*tauMinus->p4().M()*drxy_minusMC/tauMinus->pt();
    }
    ATH_MSG_INFO("ctau_minusMC " << ctau_minusMC);

    if (tauPlus) {
      tau_px_plus = tauPlus->p4().Px();
      tau_py_plus = tauPlus->p4().Py();
      tau_pz_plus = tauPlus->p4().Pz();
      tau_x_plus = tauPlus->decayVtx()->x();
      tau_y_plus = tauPlus->decayVtx()->y();
      tau_z_plus = tauPlus->decayVtx()->z();
      dx_plusMC = tauPlus->decayVtx()->x() - Boson->decayVtx()->x();
      dy_plusMC = tauPlus->decayVtx()->y() - Boson->decayVtx()->y();
      dz_plusMC = tauPlus->decayVtx()->z() - Boson->decayVtx()->z();
      double drxy_plusMCsq = dx_plusMC*dx_plusMC + dy_plusMC*dy_plusMC;
      double drxy_plusMC = (drxy_plusMCsq>0.) ? sqrt(drxy_plusMCsq) : 0;
      ctau_plusMC = CONST*tauPlus->p4().M()*drxy_plusMC/tauPlus->pt();
    }
    ATH_MSG_INFO("ctau_plusMC " << ctau_plusMC);



    for( auto track : *tracks) {
      const xAOD::TruthParticle* tTP = 0;
      if (track->isAvailable< ElementLink< xAOD::TruthParticleContainer > >("truthParticleLink") ) {
        auto link = track->auxdataConst<ElementLink< xAOD::TruthParticleContainer >>("truthParticleLink");
        if (link.isValid()) tTP = *link;
        if (tauMinusChild1 && tTP == tauMinusChild1) {
          ANA_MSG_INFO ("track is matched to tauMinusChild1");
          ANA_MSG_INFO ("reco pt " << track->pt() << " eta " << track->eta() << " phi " << track->phi() << " charge " << track->charge());
          ANA_MSG_INFO ("MC   pt " << tTP->pt()   << " eta " << tTP->eta()   << " phi " << tTP->phi()   << " pdgId " << tTP->pdgId());
          if (tauMinusChild1TP) {
            ATH_MSG_INFO("there is a track already associated to tauMinusChild1TP");
            double dR_old_minus1 = deltaR(tauMinusChild1,tauMinusChild1TP);
            double dR_new_minus1 = deltaR(tauMinusChild1,track);
            if (dR_new_minus1 <= dR_old_minus1) tauMinusChild1TP = track;
          } else {
            tauMinusChild1TP = track;
          }
          rec_pt_tauMinusChild1 = track->pt()*0.001;
          rec_eta_tauMinusChild1 = track->eta();
          rec_phi_tauMinusChild1 = track->phi();
          rec_charge_tauMinusChild1 = track->charge();
          rec_tauMinusChild1 = 1;
        } // loop for tau minus child #1
	if (tauMinusChild2 && tTP == tauMinusChild2) {
          ANA_MSG_INFO ("track is matched to tauMinusChild2");
          ANA_MSG_INFO ("reco pt " << track->pt() << " eta " << track->eta() << " phi " << track->phi() << " charge " << track->charge());
          ANA_MSG_INFO ("MC   pt " << tTP->pt()   << " eta " << tTP->eta()   << " phi " << tTP->phi()   << " pdgId " << tTP->pdgId());
          if (tauMinusChild2TP) {
            ATH_MSG_INFO("there is a track already associated to tauMinusChild2TP");
            double dR_old_minus2 = deltaR(tauMinusChild2,tauMinusChild2TP);
            double dR_new_minus2 = deltaR(tauMinusChild2,track);
            if (dR_new_minus2 <= dR_old_minus2) tauMinusChild2TP = track;
          } else {
            tauMinusChild2TP = track;
          }
          rec_pt_tauMinusChild2 = track->pt()*0.001;
          rec_eta_tauMinusChild2 = track->eta();
          rec_phi_tauMinusChild2 = track->phi();
          rec_charge_tauMinusChild2 = track->charge();
          rec_tauMinusChild2 = 1;
	} // loop for tau minus child #2
	if (tauMinusChild3 && tTP == tauMinusChild3) {
          ANA_MSG_INFO ("track is matched to tauMinusChild3");
          ANA_MSG_INFO ("reco pt " << track->pt() << " eta " << track->eta() << " phi " << track->phi() << " charge " << track->charge());
          ANA_MSG_INFO ("MC   pt " << tTP->pt()   << " eta " << tTP->eta()   << " phi " << tTP->phi()   << " pdgId " << tTP->pdgId());
          if (tauMinusChild3TP) {
            ATH_MSG_INFO("there is a track already associated to tauMinusChild3TP");
            double dR_old_minus3 = deltaR(tauMinusChild3,tauMinusChild3TP);
            double dR_new_minus3 = deltaR(tauMinusChild3,track);
            if (dR_new_minus3 <= dR_old_minus3) tauMinusChild3TP = track;
          } else {
            tauMinusChild3TP = track;
          }
          rec_pt_tauMinusChild3 = track->pt()*0.001;
          rec_eta_tauMinusChild3 = track->eta();
          rec_phi_tauMinusChild3 = track->phi();
          rec_charge_tauMinusChild3 = track->charge();
          rec_tauMinusChild3 = 1;
        } // loop for tau minus child #3          

	if (tauPlusChild1 && tTP == tauPlusChild1) {
          ANA_MSG_INFO ("track is matched to tauPlusChild1");
          ANA_MSG_INFO ("reco pt " << track->pt() << " eta " << track->eta() << " phi " << track->phi() << " charge " << track->charge());
          ANA_MSG_INFO ("MC   pt " << tTP->pt()   << " eta " << tTP->eta()   << " phi " << tTP->phi()   << " pdgId " << tTP->pdgId());
          if (tauPlusChild1TP) {
            ATH_MSG_INFO("there is a track already associated to tauPlusChild1TP");
            double dR_old_plus1 = deltaR(tauPlusChild1,tauPlusChild1TP);
            double dR_new_plus1 = deltaR(tauPlusChild1,track);
            if (dR_new_plus1 <= dR_old_plus1) tauPlusChild1TP = track;
          } else {
            tauPlusChild1TP = track;
          }
          rec_pt_tauPlusChild1 = track->pt()*0.001;
          rec_eta_tauPlusChild1 = track->eta();
          rec_phi_tauPlusChild1 = track->phi();
          rec_charge_tauPlusChild1 = track->charge();
          rec_tauPlusChild1 = 1;
	} // loop for tau plus child #1
	if (tauPlusChild2 && tTP == tauPlusChild2) {
          ANA_MSG_INFO ("track is matched to tauPlusChild2");
          ANA_MSG_INFO ("reco pt " << track->pt() << " eta " << track->eta() << " phi " << track->phi() << " charge " << track->charge());
          ANA_MSG_INFO ("MC   pt " << tTP->pt()   << " eta " << tTP->eta()   << " phi " << tTP->phi()   << " pdgId " << tTP->pdgId());
          if (tauPlusChild2TP) {
            ATH_MSG_INFO("there is a track already associated to tauPlusChild2TP");
            double dR_old_plus2 = deltaR(tauPlusChild2,tauPlusChild2TP);
            double dR_new_plus2 = deltaR(tauPlusChild2,track);
            if (dR_new_plus2 <= dR_old_plus2) tauPlusChild2TP = track;
          } else {
            tauPlusChild2TP = track;
          }
          rec_pt_tauPlusChild2 = track->pt()*0.002;
          rec_eta_tauPlusChild2 = track->eta();
          rec_phi_tauPlusChild2 = track->phi();
          rec_charge_tauPlusChild2 = track->charge();
          rec_tauPlusChild2 = 2;
        } // loop for tau plus child #2  
	if (tauPlusChild3 && tTP == tauPlusChild3) {
          ANA_MSG_INFO ("track is matched to tauPlusChild3");
          ANA_MSG_INFO ("reco pt " << track->pt() << " eta " << track->eta() << " phi " << track->phi() << " charge " << track->charge());
          ANA_MSG_INFO ("MC   pt " << tTP->pt()   << " eta " << tTP->eta()   << " phi " << tTP->phi()   << " pdgId " << tTP->pdgId());
          if (tauPlusChild3TP) {
            ATH_MSG_INFO("there is a track already associated to tauPlusChild3TP");
            double dR_old_plus3 = deltaR(tauPlusChild3,tauPlusChild3TP);
            double dR_new_plus3 = deltaR(tauPlusChild3,track);
            if (dR_new_plus3 <= dR_old_plus3) tauPlusChild3TP = track;
          } else {
            tauPlusChild3TP = track;
          }
          rec_pt_tauPlusChild3 = track->pt()*0.003;
          rec_eta_tauPlusChild3 = track->eta();
          rec_phi_tauPlusChild3 = track->phi();
          rec_charge_tauPlusChild3 = track->charge();
          rec_tauPlusChild3 = 3;
        } // loop for tau plus child #3       
      }
    } // loop over tracks

    const TLorentzVector tauMnusLVec = tauMinus->p4();
    const TLorentzVector tauPlusLVec = tauPlus->p4();

    Double_t dphi = tauMnusLVec.DeltaPhi( tauPlusLVec );
    ATH_MSG_INFO("delta phi between taus " << dphi);

    r_evtNumber = eventInfo->eventNumber();
    r_runNumber = eventInfo->runNumber();
    delta_phi_taus = dphi;
    pt_tau_minus = tauMinus->pt();
    pt_tau_plus = tauPlus->pt();
    mc_tree->Fill();
    
  } // isMC

  return EL::StatusCode::SUCCESS;
}


EL::StatusCode MyxAODAnalysis :: postExecute ()
{
  // Here you do everything that needs to be done after the main event
  // processing.  This is typically very rare, particularly in user
  // code.  It is mainly used in implementing the NTupleSvc.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode MyxAODAnalysis :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.  This is different from histFinalize() in that it only
  // gets called on worker nodes that processed input events.

  Info("finalize()", "number of events = %i", m_eventCounter );
  //Info("finalize()", "number of tracks = %i", m_trackCounter );

  ATH_MSG_INFO("Saw: "      << m_counter_eventsConsidered                                                             << " events with >= 1 combination");
  ATH_MSG_INFO("Selected: " << m_counter_eventsSelected - m_counter_eventsRejected                                    << " events with >= 1 combination");


  return EL::StatusCode::SUCCESS;
}



EL::StatusCode MyxAODAnalysis :: histFinalize ()
{
  // This method is the mirror image of histInitialize(), meaning it
  // gets called after the last event has been processed on the worker
  // node and allows you to finish up any objects you created in
  // histInitialize() before they are written to disk.  This is
  // actually fairly rare, since this happens separately for each
  // worker node.  Most of the time you want to do your
  // post-processing on the submission node after all your histogram
  // outputs have been merged.  This is different from finalize() in
  // that it gets called on all worker nodes regardless of whether
  // they processed input events.
  return EL::StatusCode::SUCCESS;
}


TVector3 MyxAODAnalysis::pca(TVector3& sv, TVector3& pv, TVector3& mom)
{ 
  float p2 = mom.Mag2();
  float pdr = mom.Dot(sv-pv);
  float t = pdr/p2;
  TVector3 tmp = t*mom;
  return sv - tmp;
}

float MyxAODAnalysis::cosTheta(TVector3& sv, TVector3& pv, TVector3& mom)
{
  return (mom.Dot(sv-pv))/(mom.Mag()*(sv-pv).Mag());
}

float MyxAODAnalysis::cosTheta_xy(TVector3& sv, TVector3& pv, TVector3& mom)
{
  return (mom.X()*(sv-pv).X()+mom.Y()*(sv-pv).Y())/(mom.Perp()*(sv-pv).Perp());
}

float MyxAODAnalysis::a0xy(TVector3& sv, TVector3& pv, TVector3& mom)
{
  float cosineTheta_xy = cosTheta_xy(sv,pv,mom);
  float sinTheta_xy = ((1.-cosineTheta_xy*cosineTheta_xy)>0.) ? sqrt((1.-cosineTheta_xy*cosineTheta_xy)) : 0.;
  return (sv-pv).Perp()*sinTheta_xy;
}

float MyxAODAnalysis::a0(TVector3& sv, TVector3& pv, TVector3& mom)
{
  float cosineTheta = cosTheta(sv,pv,mom);
  float sinTheta = ((1.-cosineTheta*cosineTheta)>0.) ? sqrt((1.-cosineTheta*cosineTheta)) : 0.;
  return (sv-pv).Mag() * sinTheta;
}

float MyxAODAnalysis::a0z(TVector3& sv, TVector3& pv, TVector3& mom)
{
  float p2 = mom.Mag2();
  float pdr = mom.Dot(sv-pv);
  float t = pdr/p2;
  TVector3 tmp = t*mom;
  TVector3 ca_point = sv - tmp;
  TVector3 a0_vec = pv - ca_point;
  return a0_vec.Z();
}

double MyxAODAnalysis::deltaR(const xAOD::TruthParticle* truth, const xAOD::TrackParticle* trk)
{
  double dEta = 0., dPhi = 0., dR = 0.;
  dEta = trk->eta() - truth->eta();
  dPhi = trk->phi() - truth->phi();
  while ( fabs(dPhi) > M_PI ) dPhi += ( dPhi > 0 ) ? -2*M_PI : 2*M_PI;
  double dRsq = dEta*dEta + dPhi*dPhi;
  dR = (dRsq>0.) ? sqrt(dRsq) : 0;
  return dR;
}

double MyxAODAnalysis::deltaR(const xAOD::TruthParticle* truth, TVector3& mom)
{
  double dEta = 0., dPhi = 0., dR = 0.;
  dEta = mom.Eta() - truth->eta();
  dPhi = mom.Phi() - truth->phi();
  while ( fabs(dPhi) > M_PI ) dPhi += ( dPhi > 0 ) ? -2*M_PI : 2*M_PI;
  double dRsq = dEta*dEta + dPhi*dPhi;
  dR = (dRsq>0.) ? sqrt(dRsq) : 0;
  return dR;
}

int MyxAODAnalysis::index_deltaR(const xAOD::TruthParticle* truth, std::vector<const xAOD::Vertex*>& selectedParticles, double& min_dR)
{
  int isel = 0, index = 0;
  for (auto x: selectedParticles) {
    xAOD::BPhysHelper jpsi_helper(x);
    TLorentzVector ref_track1 = jpsi_helper.refTrk(0, 105.65837);
    TLorentzVector ref_track2 = jpsi_helper.refTrk(1, 105.65837);
    TLorentzVector ref_track3 = jpsi_helper.refTrk(2, 105.65837);
    TLorentzVector ref_track4 = jpsi_helper.refTrk(3, 105.65837);
    TLorentzVector ref_X = ref_track1+ref_track2+ref_track3+ref_track4;
    TVector3 mcv = ref_X.Vect();
    double dR = deltaR(truth,mcv);
    if (dR < min_dR) {
      min_dR = dR;
      index = isel;
    }
    isel++;
  }
  return index;
}


const xAOD::TruthParticle* MyxAODAnalysis::mc_daughter(const xAOD::TruthParticle* parent)
{
  const xAOD::TruthParticle* daughter = NULL;

  if (parent->decayVtx()==NULL) {
    daughter = NULL;
    return daughter;
  } else {
    size_t p_nch = (parent->decayVtx())->nOutgoingParticles();
    //ANA_MSG_INFO ("in daughter fn: mu nOutgoingParticles " << p_nch);
    const xAOD::TruthVertex * p_vertex = parent->decayVtx();
    for (size_t i_child = 0; i_child < p_nch; i_child++) {
      const xAOD::TruthParticle* daughters = p_vertex->outgoingParticle(i_child);
      //ANA_MSG_INFO ("in daughter fn: mu daughter pdgId " << daughters->pdgId() << " status " << daughters->status());
      if (daughters->pdgId() == parent->pdgId()) daughter = daughters;
    }
  }

  return daughter;
}

//void MCTruthClassifier::findParticleDaughters(const xAOD::TruthParticle* thePart, std::set<const xAOD::TruthParticle*>& daughters) const {
//
//  // Get descendants
//  const xAOD::TruthVertex* endVtx = thePart->decayVtx();
//  if(endVtx!=0){
//    for(unsigned int i=0;i<endVtx->nOutgoingParticles();i++){
//      const xAOD::TruthParticle* theDaughter=endVtx->outgoingParticle(i);
//      if( theDaughter == 0 ) continue;
//      if( theDaughter->status() == 1 && theDaughter->barcode() < 200000){
//        // Add descendants with status code 1
//        daughters.insert(theDaughter);
//      }
//      findParticleDaughters(theDaughter,daughters);
//    }
//  }
//  return;
//}

void MyxAODAnalysis::findAllDescendants(const xAOD::TruthParticle* mother, std::vector<const xAOD::TruthParticle*> &family)
{
  if (mother->pdgId()<200000) {
    family.push_back(mother);
  }
  if ( mother->decayVtx()==NULL)   {    // i.e. this is a finalState
    return;
  }
  if ( !(mother->decayVtx()) && (mother->status() != 2) )   {           // i.e. this is a finalState
    return;
  }
//  if ( (mother->decayVtx()) && (mother->status() == 1) )   {
//    return;
//  }
  // main loop: iterate over all children, call method recursively.
  size_t mnch = (mother->decayVtx())->nOutgoingParticles();
  ANA_MSG_INFO ("in findAllDescendants: mother " << mother->pdgId() << " status " << mother->status() << " number of children " << mnch);
  //const xAOD::TruthVertex * mvertex = mother->decayVtx();
  //const xAOD::TruthParticle* child(0);
  for (size_t i_child = 0; i_child < mnch; i_child++) {
    const xAOD::TruthParticle* thisChild = (mother->decayVtx())->outgoingParticle(i_child);
    size_t dnch = 0;
    if (thisChild->decayVtx()) dnch = (thisChild->decayVtx())->nOutgoingParticles();
    ANA_MSG_INFO ("in findAllDescendants: thisChild " << thisChild << " child number " << i_child << " pdg " << thisChild->pdgId() << " status " << thisChild->status() << " nchildren " << dnch);
    //child = thisChild;
    //ANA_MSG_INFO ("in findAllDescendants: child " << child);
    //findAllDescendants(child,family);
    findAllDescendants(thisChild,family);
  }
  return;
}

/*
void BPhysToolBox::findAllDescendants(const HepMC::GenParticle* mother, std::vector<const HepMC::GenParticle*> &family)
{
        
        if (mother->barcode()<200000) {
                family.push_back(mother);
        }
        
        if ( mother->end_vertex()==NULL)   {    // i.e. this is a finalState
                return;
        }
        
        if ( !(mother->end_vertex()) && (mother->status() != 2) )   {           // i.e. this is a finalState
                return;
        }
        
        HepMC::GenVertex::particle_iterator firstChild, thisChild, lastChild;
        const HepMC::GenParticle* child;
        firstChild = mother->end_vertex()->particles_begin(HepMC::children);
        lastChild = mother->end_vertex()->particles_end(HepMC::children);

        // main loop: iterate over all children, call method recursively.
        
        for( thisChild = firstChild; thisChild != lastChild++; ++thisChild)   {
                child = *thisChild;
                findAllDescendants(child,family);
        }
        
        return;
}
*/
        
